nextflow.enable.dsl = 2

// Don't overwrite global params.modules, create a copy instead and use that within the main script.
def module_params = params.modules.clone()

include { GET_AMRFINDERPLUS_DB }    from './modules/amrfinderplus/get_database/main' addParams( options: module_params['amrfinderplus'] )
include { RUN_AMRFINDERPLUS }       from './modules/amrfinderplus/run/main'          addParams( options: module_params['amrfinderplus'] )
include { HAMRONIZE_AMRFINDERPLUS } from './modules/amrfinderplus/hamronize/main'    addParams( options: module_params['amrfinderplus'] )
include { GET_CARD_DB }             from './modules/rgi/get_database/main'           addParams( options: module_params['rgi'] )
include { RUN_RGI }                 from './modules/rgi/run/main'                    addParams( options: module_params['rgi'] )
include { HAMRONIZE_RGI }           from './modules/rgi/hamronize/main'              addParams( options: module_params['rgi'] )
include { SUMMARIZE_HAMRONIZATION } from './modules/hamronize/summarize/main'        addParams( options: module_params['harmonize'] )

// Check mandatory parameters
if (params.input) { ch_input = file(params.input) } else { exit 1, 'Input not specified!' }

workflow {
    sample_id_and_seqs = Channel
        .fromPath(params.input)
        .map{ file -> tuple (file.baseName.replaceAll(/\..+$/,''), file)}
        .ifEmpty { error "Cannot find any files matching: ${params.input}" }

    if (params.amrfinderplus) {
        GET_AMRFINDERPLUS_DB()
        RUN_AMRFINDERPLUS(sample_id_and_seqs, GET_AMRFINDERPLUS_DB.out.database)
        HAMRONIZE_AMRFINDERPLUS(sample_id_and_seqs.join(RUN_AMRFINDERPLUS.out.tsv))
    }

    if (params.rgi) {
        GET_CARD_DB()
        RUN_RGI(sample_id_and_seqs, GET_CARD_DB.out.database)
        HAMRONIZE_RGI(sample_id_and_seqs.join(RUN_RGI.out.tsv))
    }

    SUMMARIZE_HAMRONIZATION(
        HAMRONIZE_AMRFINDERPLUS.out.json.map{ it[1] }
            .mix(HAMRONIZE_RGI.out.json.map{ it[1] })
            .collect()
    )
}
