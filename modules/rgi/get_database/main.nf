process GET_CARD_DB {
    container "quay.io/biocontainers/rgi:5.2.0--pyhdfd78af_0"

    output:
    path  "database", emit: database

    script:
    """
    mkdir database
    curl https://card.mcmaster.ca/download/0/broadstreet-v3.1.4.tar.bz2 --output database/card.tar.bz2
    tar -C database -xvf database/card.tar.bz2
    """
}