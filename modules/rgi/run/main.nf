process RUN_RGI {
    container "quay.io/biocontainers/rgi:5.2.0--pyhdfd78af_0"
    publishDir "${params.output}/rgi", mode: "copy"

    input:
    tuple val(id), path(sequence)
    path(database)

    output:
    tuple val(id), path("${id}.report.txt"), emit: tsv
    tuple val(id), path("${id}.report.json"), emit: json

    script:
    """
    rgi load --card_json ${database}/card.json
    rgi main --input_sequence ${sequence} --output_file ${id}.report --clean --num_threads 1
    """
}