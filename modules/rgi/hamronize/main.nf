process HAMRONIZE_RGI {
    container "quay.io/biocontainers/hamronization:1.0.3--py_0"
    publishDir "${params.output}/rgi", mode: "copy"

    input:
    tuple val(id), path(sequence), path(report)

    output:
    tuple val(id), path("${id}.hamronize_rgi.json"), emit: json

    script:
    """
    hamronize rgi   --analysis_software_version 5.2 \
                    --reference_database_version 3.1.4 \
                    --input_file_name ${sequence} \
                    --format json \
                    ${report} \
                    > ${id}.hamronize_rgi.json
    """
}