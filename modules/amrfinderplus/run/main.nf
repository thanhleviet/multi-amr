process RUN_AMRFINDERPLUS {
    container "quay.io/biocontainers/ncbi-amrfinderplus:3.10.16--h17dc2d4_0"
    publishDir "${params.output}/amrfinderplus", mode: "copy"

    input:
    tuple val(id), path(sequence)
    path(database)

    output:
    tuple val(id), path("${id}.report.tsv"), emit: tsv

    script:
    """
    amrfinder -n ${sequence} -o ${id}.report.tsv -d ${database}/latest
    """
}