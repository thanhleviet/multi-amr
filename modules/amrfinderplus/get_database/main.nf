process GET_AMRFINDERPLUS_DB {
    container "quay.io/biocontainers/ncbi-amrfinderplus:3.10.16--h17dc2d4_0"

    output:
    path  "database", emit: database

    script:
    """
    mkdir database
    amrfinder_update -d database
    """
}
